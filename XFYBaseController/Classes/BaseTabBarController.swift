//
//  BaseTabBarController.swift
//  Pods-XFYBaseController_Example
//
//  Created by 🐑 on 2019/3/15.
//

import UIKit

open class BaseTabBarController: UITabBarController {
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initView()
    }
}

// MARK: - 私有方法
private extension BaseTabBarController {
    
    // MARK: - 初始化
    func initView() {
        view.backgroundColor = .white
        tabBar.tintColor = ControllerManager.shared.tabBarTintColor
        // iOS12.1闪动Bug特殊处理（副作用：没有磨砂效果）
        tabBar.isTranslucent = false
    }
}
