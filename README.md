# XFYBaseController

[![CI Status](https://img.shields.io/travis/leonazhu/XFYBaseController.svg?style=flat)](https://travis-ci.org/leonazhu/XFYBaseController)
[![Version](https://img.shields.io/cocoapods/v/XFYBaseController.svg?style=flat)](https://cocoapods.org/pods/XFYBaseController)
[![License](https://img.shields.io/cocoapods/l/XFYBaseController.svg?style=flat)](https://cocoapods.org/pods/XFYBaseController)
[![Platform](https://img.shields.io/cocoapods/p/XFYBaseController.svg?style=flat)](https://cocoapods.org/pods/XFYBaseController)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

XFYBaseController is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'XFYBaseController'
```

## Author

leonazhu, 412038671@qq.com

## License

XFYBaseController is available under the MIT license. See the LICENSE file for more info.
