//
//  ViewController.swift
//  XFYBaseController
//
//  Created by leonazhu on 03/14/2019.
//  Copyright (c) 2019 leonazhu. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {
    
    var addRightButton: Bool = true
    var navigationBarHidden: Bool = false
    var statusBarStyle: UIStatusBarStyle = .lightContent
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        initView()
    }
    
    override var isNavigationBarHidden: Bool {
        return navigationBarHidden
    }
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    
    // 隐藏状态栏
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - 操作
    @IBAction func pop(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func test1(_ sender: UIButton) {
        let controller = fromStoryboard(hidesBottomBarWhenPushed: true)
        controller.title = sender.currentTitle
        navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func test2(_ sender: UIButton) {
        let controller = fromStoryboard(hidesBottomBarWhenPushed: false)
        controller.title = sender.currentTitle
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func test3(_ sender: UIButton) {
        let controller = fromStoryboard(hidesBottomBarWhenPushed: true, navigationBarHidden: true)
        controller.title = sender.currentTitle
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func test4(_ sender: UIButton) {
        let controller = fromStoryboard(hidesBottomBarWhenPushed: true)
        controller.title = sender.currentTitle
        let navigationController = BaseNavigationController(rootViewController: controller)
        present(navigationController, animated: true)
    }
    
    @IBAction func test5(_ sender: UIButton) {
        let controller = fromStoryboard(hidesBottomBarWhenPushed: true, statusBarStyle: .default)
        controller.title = sender.currentTitle
        let navigationController = BaseNavigationController(rootViewController: controller)
        present(navigationController, animated: true)
    }
    
    @IBAction func test6(_ sender: UIButton) {
        let controller = fromStoryboard(hidesBottomBarWhenPushed: true, statusBarStyle: .default)
        controller.title = sender.currentTitle
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func test7(_ sender: UIButton) {
        let controller = fromStoryboard(hidesBottomBarWhenPushed: true, navigationBarHidden: true, statusBarStyle: .default)
        controller.title = sender.currentTitle
        navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: - 方法
    func fromStoryboard(hidesBottomBarWhenPushed: Bool = true, navigationBarHidden: Bool = false, statusBarStyle: UIStatusBarStyle = .lightContent) -> ViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController else {
            return ViewController()
        }
        controller.hidesBottomBarWhenPushed = hidesBottomBarWhenPushed
        controller.navigationBarHidden = navigationBarHidden
        controller.statusBarStyle = statusBarStyle
        return controller
    }
}

// MARK: - 私有方法
extension ViewController {
    
    func initView() {
        if addRightButton {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "测试", style: .done, target: nil, action: nil)
        }
    }
}

