//
//  ScrollViewController.swift
//  XFYBaseController_Example
//
//  Created by 🐑 on 2019/5/21.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class ScrollViewController: BaseViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }
}

// MARK: - 私有
private extension ScrollViewController {
    
    func initView() {
        guard let gestureArray = navigationController?.view.gestureRecognizers else { return }
        for gesture in gestureArray {
            if gesture.isKind(of: UIScreenEdgePanGestureRecognizer.self) {
                scrollView.panGestureRecognizer.require(toFail: gesture)
                break
            }
        }
    }
}
