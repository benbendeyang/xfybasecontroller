//
//  ControllerManager.swift
//  Pods-XFYBaseController_Example
//
//  Created by 🐑 on 2019/3/15.
//

import Foundation

open class ControllerManager {
    
    public static let shared = ControllerManager()
    
    public private(set) var tabBarTintColor: UIColor?
    public private(set) var navigationBarTintColor: UIColor?
    public private(set) var navigationBarBarTintColor: UIColor?
    public private(set) var navigationTitleColor: UIColor = .white
    public private(set) var navigationTitleFont: UIFont = .boldSystemFont(ofSize: 18)
    public private(set) var backImage: UIImage = UIImage()
    public private(set) var statusBarStyle: UIStatusBarStyle = .lightContent
}

// MARK: - 公共方法，需要在AppDelegate启动时设置
public extension ControllerManager {
    
    func setup(mainColor: UIColor, navigationTintColor: UIColor = .white, navigationTitleFont: UIFont = .boldSystemFont(ofSize: 18), backImage: UIImage, statusBarStyle: UIStatusBarStyle = .lightContent) {
        self.tabBarTintColor = mainColor
        self.navigationBarBarTintColor = mainColor
        self.navigationBarTintColor = navigationTintColor
        self.navigationTitleColor = navigationTintColor
        self.navigationTitleFont = navigationTitleFont
        self.backImage = backImage
        self.statusBarStyle = statusBarStyle
    }
    
    func setup(tabBarTintColor: UIColor, navigationBarTintColor: UIColor, navigationBarBarTintColor: UIColor, navigationTitleColor: UIColor, navigationTitleFont: UIFont, backImage: UIImage, statusBarStyle: UIStatusBarStyle = .lightContent) {
        self.tabBarTintColor = tabBarTintColor
        self.navigationBarTintColor = navigationBarTintColor
        self.navigationBarBarTintColor = navigationBarBarTintColor
        self.navigationTitleColor = navigationTitleColor
        self.navigationTitleFont = navigationTitleFont
        self.backImage = backImage
        self.statusBarStyle = statusBarStyle
    }
}
