//
//  BaseNavigationController.swift
//  Pods-XFYBaseController_Example
//
//  Created by 🐑 on 2019/3/15.
//

import UIKit

open class BaseNavigationController: UINavigationController {
    
    var popDelegate: UIGestureRecognizerDelegate?
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initView()
    }
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        if let style = topViewController?.preferredStatusBarStyle {
            return style
        } else {
            return ControllerManager.shared.statusBarStyle
        }
    }
    
    deinit {
        #if DEBUG
        print("\(type(of: self)) --> deinit")
        #endif
        
    }
}

// MARK: - 私有方法
private extension BaseNavigationController {
    
    // MARK: - 初始化
    func initView() {
        let baseManager = ControllerManager.shared
        delegate = self
        popDelegate = interactivePopGestureRecognizer?.delegate
        view.backgroundColor = .white
        navigationBar.tintColor = baseManager.navigationBarTintColor
        navigationBar.barTintColor = baseManager.navigationBarBarTintColor
        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: baseManager.navigationTitleColor, NSAttributedString.Key.font: baseManager.navigationTitleFont]
    }
    
    @objc func pop() {
        popViewController(animated: true)
    }
}

// MARK: - 协议
extension BaseNavigationController: UINavigationControllerDelegate {
    
    public func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        let isRootContoller = (viewController == navigationController.viewControllers.first)
        if !isRootContoller {
            viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(image: ControllerManager.shared.backImage, style: .done, target: self, action: #selector(pop))
        }
    }
    
    public func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        let isRootContoller = (viewController == navigationController.viewControllers.first)
        if isRootContoller {
            interactivePopGestureRecognizer?.delegate = popDelegate
        } else {
            interactivePopGestureRecognizer?.delegate = nil
        }
    }
}
