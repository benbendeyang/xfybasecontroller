//
//  BaseViewController.swift
//  Pods-XFYBaseController_Example
//
//  Created by 🐑 on 2019/3/14.
//

import UIKit

open class BaseViewController: UIViewController {

    open var isNavigationBarHidden: Bool {
        return false
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return ControllerManager.shared.statusBarStyle
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(isNavigationBarHidden, animated: true)
    }
    
    deinit {
        #if DEBUG
        print("\(type(of: self)) --> deinit")
        #endif
    }
}
